from setuptools import find_packages, setup

setup(
	name='vowel-recognition',
	packages=find_packages(),
	version='0.0.1',
	description='Find vowels in a live microphone input stream',
	author='Sorunome',
	license='Apache 2',
	install_requires=['pyaudio', 'scipy', 'numpy', 'librosa'],
)
