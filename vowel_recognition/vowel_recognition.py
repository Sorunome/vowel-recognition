import pyaudio
import numpy as np
import scipy as sc
import struct
from collections import deque
import threading
import time
import librosa

#info = audio.get_host_api_info_by_index(0)
#numdevices = info.get('deviceCount')
#for i in range(0, numdevices):
#    if (audio.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
#        print("Input Device id ", i, " - ", audio.get_device_info_by_host_api_device_index(0, i).get('name'))
class VowelRecognition:
	format = pyaudio.paFloat32
	channels = 1
	stream = None
	audio = None
	is_running = False
	thread = None
	on_mfccs = None
	on_vol = None
	on_vowels = None
	on_max_vowel = None
	def __init__(self, vowels, on_vowels = None, on_max_vowel = None, bg_threashold = 5, on_vol = None, rate = 48000):
		self.rate = rate
		self.chunk = 512
		self.vowels = vowels
		self.on_vowels = on_vowels
		self.on_max_vowel = on_max_vowel
		self.on_vol = on_vol
		self.bg_threashold = bg_threashold
		self.buffer = self.rate // 16

	def start(self):
		self.stop()
		self.audio = pyaudio.PyAudio()
		self.stream = self.audio.open(
			format = self.format,
			channels = self.channels,
			rate = self.rate,
			input = True,
			frames_per_buffer = self.chunk,
#			input_device_index = 10,
		)
		self.is_running = True
		self.thread = threading.Thread(target=self.loop)
		self.thread.start()

	def calibrate(self):
		mfccs = []
		def on_mfccs(d):
			mfccs.append(d)
		self.on_mfccs = on_mfccs
		self.start()
		time.sleep(3)
		self.stop()
		mfccs = np.swapaxes(mfccs, 0, 1)
		return [float(np.average(r)) for r in mfccs]

	def stop(self):
		self.is_running = False
		if self.thread is not None:
			self.thread.join()
			self.thread = None
		if self.stream is not None:
			self.stream.stop_stream()
			self.stream.close()
			self.stream = None
		if self.audio is not None:
			self.audio.terminate()
			self.audio = None

	def get_mfccs(self, buff):
		ret = librosa.feature.melspectrogram(y=buff, sr=self.rate, n_mels=12, fmax=2000)
#		print("a")
		vol = np.sum(ret)
		ret = librosa.power_to_db(ret, ref=np.max)
#		print("b")
		ret = list(map(np.average, ret))
	
		if self.on_mfccs is not None:
			self.on_mfccs(ret)
		return ret, vol

	last_vowel = ''
	last_vowels = deque(['']*2, maxlen = 2)
	def analyze_vowels(self, d, vol):
		ret = {}
		for v in self.vowels.items():
			ret[v[0]] = sc.spatial.distance.cosine(v[1], d)
		if ret['bg'] is not None:
			if vol < self.bg_threashold:
				ret['bg'] /= 10
			else:
				ret['bg'] *= 10
		if self.on_vowels is not None:
			self.on_vowels(ret)
		if self.on_vol is not None:
			self.on_vol(vol)
		if self.on_max_vowel is None:
			return
		min = 999
		minV = ''
		for v, d in ret.items():
			if d < min:
				min = d
				minV = v
		# minV now holds the most prominent vowel.
		# time to do some more analysis to remove "flickering"
		last_vowel_in = self.last_vowel in self.last_vowels 
		self.last_vowels.append(minV)
		if np.all(self.last_vowels == self.last_vowels[0]):
			if self.last_vowel != minV:
				self.last_vowel = minV
				self.on_max_vowel(minV)
		elif not last_vowel_in:
			if self.last_vowel != minV:	
				self.last_vowel = minV
				self.on_max_vowel(minV)

	def loop(self):
		buff = deque([0]*self.buffer, maxlen=self.buffer)
		format = 'f' * self.chunk
		while self.is_running and len(raw_data := self.stream.read(self.chunk)):
#			print(1)
			buff.extend(struct.unpack(format, raw_data))
#			print(2)
			mfccs, vol = self.get_mfccs(np.array(buff))
#			print(3)
			self.analyze_vowels(mfccs, vol)
#			print(4)
			

if __name__ == '__main__':
	import argparse
	import yaml
	parser = argparse.ArgumentParser(
		prog = 'Vowel Recognition',
		description = 'Recognise vowels said on mic input stream',
	)
	parser.add_argument('--calibrate', help='Calibrate a vowel')
	parser.add_argument('--uncalibrate', help='Remove a vowel from calibration')
	parser.add_argument('-c', '--config', help='Config file to use', default='config.yaml')
	args = parser.parse_args()
	with open(args.config, 'r') as f: config = yaml.safe_load(f)
	if config is None:
		config = {
			'vowels': {},
		}

	def vowel_callback(v):
		print(v)

	v = VowelRecognition(config['vowels'], on_max_vowel = vowel_callback)
	if args.calibrate is not None:
		vowel = args.calibrate
		mfccs = v.calibrate()
		config['vowels'][vowel] = mfccs
		with open(args.config, 'w') as f: yaml.dump(config, f, default_flow_style=False)
	elif args.uncalibrate is not None:
		del config['vowels'][args.uncalibrate]
		with open(args.config, 'w') as f: yaml.dump(config, f, default_flow_style=False)
	else:
		v.start()
		try:
			while True:
				time.sleep(1)
		finally:
			v.stop()
